# SPDX-FileCopyrightText: 2024 Linutronix GmbH
# SPDX-License-Identifier: CC0-1.0
default:
  image: python:3.11-bookworm

#
# GitLab's predefined variables are defined at:
# https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
#
variables:
  GIT_DEPTH: 50
  OUTPUT_DIR: output
  HEADERS_DIR: ${OUTPUT_DIR}/individual-headers
  KCPUID_CSV_FILE: ${OUTPUT_DIR}/kcpuid.csv
  KERNEL_C_HEADER: ${OUTPUT_DIR}/cpuid-bitfields.h

before_script:
  - apt-get -y update
  - mkdir -p ${OUTPUT_DIR}/
  - mkdir -p ${HEADERS_DIR}/

#
# Don't waste computing resources: Run this pipeline only if it's the
# main branch, the tip branch, or if the branch's name ends with "-ci".
# https://docs.gitlab.com/ee/ci/yaml/workflow.html
#
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^(main|tip|.*-ci)$/
      when: always
    - when: never

validate_REUSE_v3_compliance:
  stage: .pre
  script:
    - apt-get -y install reuse
    - reuse lint

validate_xml_schema:
  stage: .pre
  script:
    - apt-get -y install libxml2-utils
    - ./scripts/validate_xml.sh

check_xml_codingstyle:
  stage: .pre
  script:
    - apt-get -y install gawk
    - ./scripts/check_xml_style.awk

#
# Spellcheck and enforce a consistent x86 terminology style for
# all of the XML database's cpuid bitfield descriptions.
#
spellcheck_xml_attributes:
  stage: .pre
  script:
    - apt-get -y install libxml2-utils hunspell hunspell-en-us
    - ./scripts/spellcheck_xml.sh --hunspell

# pyproject.toml already sets 'tool.mypy.strict'
run_mypy_strict_typing_checks:
  stage: test
  script:
    - apt-get -y install mypy
    - mypy tools/xmlgen.py tools/cpuidgen.py

compile_individual_kernel_headers:
  stage: test
  script:
    - apt-get -y install gcc python3-poetry
    - poetry install
    - OUTPUT_DIR=${HEADERS_DIR} ./scripts/generate_kernel_headers.sh
    - >
      for header in ${HEADERS_DIR}/leaf_*.h; do
        sed -i 's/u32/__u32/g' $header
        echo "Compile-testing header file ${header}"
        gcc -Wall $header
      done

compile_jumbo_kernel_header:
  stage: test
  script:
    - apt-get -y install gcc python3-poetry
    - poetry install
    - poetry run cpuidgen --kheaders > ${KERNEL_C_HEADER}
    - sed -i 's/u32/__u32/g' ${KERNEL_C_HEADER}
    - echo "Compile-testing header file ${KERNEL_C_HEADER}"
    - gcc -Wall ${KERNEL_C_HEADER}
  artifacts:
    paths:
      - ${KERNEL_C_HEADER}

deploy_kcpuid_csv_file:
  stage: deploy
  script:
    - apt-get -y install python3-poetry
    - poetry install
    - poetry run cpuidgen --kcpuid > ${KCPUID_CSV_FILE}
  artifacts:
    paths:
      - ${KCPUID_CSV_FILE}

deploy_individual_kernel_headers:
  stage: deploy
  script:
    - apt-get -y install python3-poetry
    - poetry install
    - OUTPUT_DIR=${HEADERS_DIR} ./scripts/generate_kernel_headers.sh
  artifacts:
    paths:
      - ${HEADERS_DIR}/leaf_*.h

deploy_jumbo_kernel_header:
  stage: deploy
  script:
    - apt-get -y install gcc python3-poetry
    - poetry install
    - poetry run cpuidgen --kheaders > ${KERNEL_C_HEADER}
  artifacts:
    paths:
      - ${KERNEL_C_HEADER}

#
# Create a GitLab release when an annotated tag is pushed
# See https://docs.gitlab.com/ee/ci/yaml/#release
#
do_gitlab_release:
  stage: .post
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  # Override global before_script
  before_script: []
  script:
    - echo "Creating a GitLab release for $CI_COMMIT_TAG"
  release:
    name: "Release $CI_COMMIT_TAG"
    description: '$DESCRIPTION'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
